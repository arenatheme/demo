(function($)
{
	$('.landing-item-inner').hover(function()
	{
		var $img = $(this).find('img'),
		imgHtoAnimate = $img.height() - $(this).height();
		$(this).find('img').stop().animate({marginTop: -(imgHtoAnimate)}, 1000);
	},
	function()
	{
		$(this).find('img').stop().animate({marginTop: 0},300);
	});
})(jQuery);