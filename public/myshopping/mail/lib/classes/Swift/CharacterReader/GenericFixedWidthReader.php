.          0 �|y�F�F  |y�F�_    ..          ��y�F�F  �y�F�_    Cp   ������ �������������  ����i d t h R  �e a d e r .   p h G e n e r  �i c F i x e   d W GENERI~1PHP  �|y�F�F  .��F�_@	  B. p h p    �������������  ����U s A s c  �i i R e a d   e r USASCI~1PHP  �|y�F�F  .��F�_�  Bp   ������ �������������  ����U t f 8 R  �e a d e r .   p h UTF8RE~1PHP  f|y�F�F  .��F�_�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  lic function getCharPositions($string, $startOffset, &$currentMap, &$ignoredChars)
    {
        $strlen = strlen($string);
        // % and / are CPU intensive, so, maybe find a better way
        $ignored = $strlen % $this->_width;
        $ignoredChars = substr($string, - $ignored);
        $currentMap = $this->_width;

        return ($strlen - $ignored) / $this->_width;
    }

    /**
     * Returns the mapType.
     *
     * @return int
     */
    public function getMapType()
    {
        return self::MAP_TYPE_FIXED_LEN;
    }

    /**
     * Returns an integer which specifies how many more bytes to read.
     *
     * A positive integer indicates the number of more bytes to fetch before invoking
     * this method again.
     *
     * A value of zero means this is already a valid character.
     * A value of -1 means this cannot possibly be a valid character.
     *
     * @param string  $bytes
     * @param int     $size
     *
     * @return int
     */
    public function validateByteSequence($bytes, $size)
    {
        $needed = $this->_width - $size;

        return ($needed > -1) ? $needed : -1;
    }

    /**
     * Returns the number of bytes which should be read to start each character.
     *
     * @return int
     */
    public function getInitialByteSize()
    {
        return $this->_width;
    }
}
